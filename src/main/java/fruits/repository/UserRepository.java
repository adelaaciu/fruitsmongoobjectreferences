package fruits.repository;

import fruits.entity.User;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<User, String> {

	User findByEmailAndPassword(String email, String password);

	User findByEmail(String email);

	void deleteByEmail(String email);
}
