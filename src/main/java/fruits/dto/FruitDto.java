package fruits.dto;

public class FruitDto {
	private String name;
	private int quantity;

	public FruitDto() {
	}

	public FruitDto(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
}
