package fruits.dto;

import java.util.List;

public class CartDto {
	private List<FruitDto> fruits;

	public List<FruitDto> getFruits() {
		return fruits;
	}

	public void setFruits(List<FruitDto> fruits) {
		this.fruits = fruits;
	}
}
