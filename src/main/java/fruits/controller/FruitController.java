package fruits.controller;

import fruits.dto.FruitDto;
import fruits.service.FruitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/fruits")
public class FruitController {
	@Autowired
	private FruitService fruitService;

	@GetMapping(value = "/getFruits")
	public List<FruitDto> getFruitsStringList() {
		return fruitService.getFruits();
	}

	@PostMapping(value = "/addFruit")
	public ResponseEntity<FruitDto> addFruit(@RequestBody FruitDto fruit) {
		FruitDto fruitDto = fruitService.addFruit(fruit);
		return ResponseEntity.ok(fruitDto);
	}

	@DeleteMapping(value = "/deleteFruit/{fruitName}")
	public ResponseEntity deleteFruit(@PathVariable String fruitName) {
		fruitService.deleteFruit(fruitName);

		return ResponseEntity.status(HttpStatus.ACCEPTED).build();
	}
}
