package fruits.controller;

import fruits.dto.CartDto;
import fruits.dto.FruitDto;
import fruits.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/cart")
public class CartController {

	@Autowired
	private CartService cartService;

	@PutMapping("/placeOrder/{userEmail}")
	public ResponseEntity placeOrder(@PathVariable String userEmail, @RequestBody CartDto cart) {
		cartService.updateCart(userEmail, cart);
		return ResponseEntity.ok().build();
	}


	@PostMapping("/addFruitToCart/{userEmail}")
	public ResponseEntity addFruitToCart(@PathVariable String userEmail, @RequestBody FruitDto fruitDto) {
		cartService.addFruitToCart(userEmail, fruitDto);
		return ResponseEntity.ok().build();
	}

	@GetMapping(value = "/getCarts")
	public List<CartDto> getFruitsStringList() {
		return cartService.getCarts();
	}
}
