package fruits.controller;

import fruits.dto.UserDto;
import fruits.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/uesrs")
public class UserController {

	@Autowired
	private UserService userService;

	@PostMapping("/addUser")
	public ResponseEntity<UserDto> saveUser(@RequestBody UserDto userDto) {
		UserDto user = userService.saveUser(userDto);

		return ResponseEntity.ok(user);
	}

	@DeleteMapping("/deleteUser/{email}")
	public ResponseEntity<UserDto> deleteUser(@PathVariable String email) {
		userService.deleteUser(email);

		return ResponseEntity.ok().build();
	}

	@GetMapping("/{email}")
	public ResponseEntity<UserDto> getUser(@PathVariable String email) {
		UserDto user = userService.getUser(email);

		return ResponseEntity.ok(user);
	}
}
