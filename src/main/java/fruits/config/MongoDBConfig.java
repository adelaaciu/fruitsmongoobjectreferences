package fruits.config;

import fruits.repository.FruitRepository;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@EnableMongoRepositories(basePackageClasses = FruitRepository.class)
public class MongoDBConfig {
}
