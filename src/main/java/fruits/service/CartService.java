package fruits.service;

import fruits.dto.CartDto;
import fruits.dto.FruitDto;
import fruits.entity.Cart;
import fruits.entity.Fruit;
import fruits.entity.User;
import fruits.repository.CartRepository;
import fruits.repository.FruitRepository;
import fruits.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CartService {
	@Autowired
	private CartRepository cartRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private FruitRepository fruitRepository;

	public void updateCart(String userEmail, CartDto dto) {
		User user = userRepository.findByEmail(userEmail);
		Optional<Cart> cart = cartRepository.findById(user.getCart().getCartId());
		List<Fruit> fruits = getFruits(dto.getFruits());

		Cart cartDoc = cart.get();
		cartDoc.setFruits(fruits);

		updateFruitsQuantity(fruits, dto.getFruits());
		cartRepository.save(cartDoc);

		user.setCart(cartDoc);

//		throw new CouldNotPlaceOrder();
	}

	private void updateFruitsQuantity(List<Fruit> fruits, List<FruitDto> fruitDtos) {
		for (Fruit fruit : fruits) {
			FruitDto fruitDto = fruitDtos.stream().filter(f -> f.getName().equals(fruit.getName())).findFirst().get();
			int newQuantity = fruit.getQuantity() - fruitDto.getQuantity();
			fruit.setQuantity(newQuantity);
			fruitRepository.save(fruit);
		}
	}

	private List<Fruit> getFruits(List<FruitDto> fruitDtos) {
		return fruitDtos
				.stream()
				.map(dto -> fruitRepository.findByName(dto.getName()))
				.collect(Collectors.toList());
	}

	private CartDto cartToDto(Cart cart) {
		CartDto dto = new CartDto();
		dto.setFruits(getFruitsDto(cart.getFruits()));

		return dto;
	}

	private List<FruitDto> getFruitsDto(List<Fruit> fruits) {
		return fruits
				.stream()
				.map(fruit -> createFruitDto(fruit))
				.collect(Collectors.toList());
	}

	private FruitDto createFruitDto(Fruit fruit) {
		FruitDto fruitDto = new FruitDto();
		fruitDto.setName(fruit.getName());
		fruitDto.setQuantity(fruit.getQuantity());

		return fruitDto;
	}

	public List<CartDto> getCarts() {
		return cartRepository.findAll()
				.stream()
				.map(cart -> cartToDto(cart))
				.collect(Collectors.toList());
	}

	public void addFruitToCart(String userEmail, FruitDto fruitDto) {
		User user = userRepository.findByEmail(userEmail);
		Optional<Cart> cart = cartRepository.findById(user.getCart().getCartId());
		Fruit fruit = fruitRepository.findByName(fruitDto.getName());

		Cart cartDoc = cart.get();
		if (cartDoc.getFruits() == null) {
			cartDoc.setFruits(new ArrayList<>());
		}

		cartDoc.getFruits().add(fruit);

		fruit.setQuantity(fruit.getQuantity() - fruitDto.getQuantity());

		fruitRepository.save(fruit);
		cartRepository.save(cartDoc);
		user.setCart(cartDoc);

		userRepository.save(user);

	}
}
