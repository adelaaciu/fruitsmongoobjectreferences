package fruits.service;

import fruits.dto.CartDto;
import fruits.dto.FruitDto;
import fruits.dto.UserDto;
import fruits.entity.Cart;
import fruits.entity.Fruit;
import fruits.entity.User;
import fruits.exception.UserAlreadyExists;
import fruits.repository.CartRepository;
import fruits.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private CartRepository cartRepository;

	public UserDto saveUser(UserDto userDto) {
		User user = convertToDocument(userDto);

		if (isUserUnique(user)) {
			Cart c = new Cart();
			Cart cart = cartRepository.save(c);
			user.setCart(cart);

			userRepository.save(user);
			return userDto;
		}
		throw new UserAlreadyExists();
	}

	private User convertToDocument(UserDto userDto) {
		User user = new User();
		user.setEmail(userDto.getEmail());
		user.setPassword(userDto.getPassword());

		return user;
	}

	private boolean isUserUnique(User user) {
		return userRepository.findByEmail(user.getEmail()) == null;
	}

	public void deleteUser(String email) {
		userRepository.deleteByEmail(email);
	}

	public UserDto getUser(String email) {
		User userDoc = userRepository.findByEmail(email);
		UserDto userDto = new UserDto();
		userDto.setEmail(userDoc.getEmail());
		userDto.setPassword(userDoc.getPassword());
		userDto.setCart(cartToDto(userDoc.getCart()));

		return userDto;
	}

	private CartDto cartToDto(Cart cart) {
		CartDto dto = new CartDto();
		dto.setFruits(getFruitsDto(cart.getFruits()));

		return dto;
	}

	private List<FruitDto> getFruitsDto(List<Fruit> fruits) {
		return fruits
				.stream()
				.map(fruit -> createFruitDto(fruit))
				.collect(Collectors.toList());
	}

	private FruitDto createFruitDto(Fruit fruit) {
		FruitDto fruitDto = new FruitDto();
		fruitDto.setName(fruit.getName());
		fruitDto.setQuantity(fruit.getQuantity());

		return fruitDto;
	}

}
