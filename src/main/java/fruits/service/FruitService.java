package fruits.service;

import fruits.dto.FruitDto;
import fruits.entity.Fruit;
import fruits.exception.FruitAlreadyExistException;
import fruits.exception.FruitNotFoundException;
import fruits.repository.FruitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class FruitService {

	@Autowired
	private FruitRepository fruitRepository;

	public List<FruitDto> getFruits() {
		List<Fruit> fruits = fruitRepository.findAll();
		List<FruitDto> fruitDtos = fruits.stream()
				.map(fruit -> toFruitDto(fruit))
				.collect(Collectors.toList());

		return fruitDtos;
	}

	public FruitDto addFruit(FruitDto fruitDto) {
		if (!getFruit(fruitDto.getName()).isPresent()) {
			Fruit fruit = toFruit(fruitDto);
			fruitRepository.save(fruit);

			return fruitDto;
		}

		throw new FruitAlreadyExistException(fruitDto.getName());
	}

	public void deleteFruit(String fruitName) {
		Optional<Fruit> fruit = getFruit(fruitName);
		if (fruit.isPresent()) {
			fruitRepository.delete(fruit.get());
			return;
		}

		throw new FruitNotFoundException(fruitName);

	}

	private Optional<Fruit> getFruit(String fruitName) {
		Fruit fruit = fruitRepository.findByName(fruitName);
		return Optional.ofNullable(fruit);
	}

	private FruitDto toFruitDto(Fruit fruit) {
		FruitDto dto = new FruitDto();
		dto.setName(fruit.getName());
		dto.setQuantity(fruit.getQuantity());

		return dto;
	}

	private Fruit toFruit(FruitDto dto) {
		Fruit fruit = new Fruit();
		fruit.setName(dto.getName());
		fruit.setQuantity(dto.getQuantity());

		return fruit;
	}

}
