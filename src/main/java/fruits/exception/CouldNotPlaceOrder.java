package fruits.exception;

public class CouldNotPlaceOrder extends RuntimeException {
	public CouldNotPlaceOrder() {
		super("Order could not be executed");
	}
}
